<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// ADMIN
//guru
Route::get('/guru', 'GuruAdmController@index')->name('guru');
Route::get('/detailguru', 'GuruAdmController@detail')->name('detailguru');

//siswa
Route::get('/siswa', 'SiswaAdmController@index')->name('siswa');

//mapel
Route::get('/mapel', 'MapelAdmController@index')->name('mapel');

//kompetensi
Route::get('/kompetensi', 'KompetensiController@index')->name('kompetensi');

//bidang
Route::get('/bidang', 'BidangStudiController@index')->name('bidang');

//kd
Route::get('/kd', 'KDController@index')->name('kd');

//kelas
Route::get('/kelas', 'KelasAdmController@index')->name('kelas');

//nilai
Route::get('/nilai', 'NilaiAdmController@index')->name('nilai');

// GURU
//profileguru
Route::get('/profileguru', 'ProfileGuruController@index')->name('profileguru');


// SISWA
//profilesiswa
Route::get('/profilesiswa', 'ProfileSiswaController@index')->name('profilesiswa');
