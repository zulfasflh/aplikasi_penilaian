<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuruAdmController extends Controller
{
    public function index()
    {
        return view('admin.guru.index');
    }

    public function detail()
    {
        return view('admin.guru.detail');
    }
}
