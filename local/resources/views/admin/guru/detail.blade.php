@extends('layouts.master')
@section('content')

    <!-- JUMBOTRON -->
    <div class="container">
    <div class="jumbotron" style="background-color: transparent;margin-top:-40px;color:#797979">

        <!-- SESSION ALERT -->
        @if(session('sukses'))
          <div class="alert alert-success" role="alert">
            {{session('sukses')}}
          </div>
	      @endif
        <!-- END ALERT -->

        <h1 class="display-5">DETAIL GURU</h1>
        <hr>
    </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- Form Detail GURU -->
    <div class="card">
        <h5 class="card-header text-center py-3" style="background-color:#506884;color:#fff;">
            <b>Nama Guru</b>
        </h5>
            <div class="card-body px-lg-5">
            <form action="" class="text-center" style="color:#757575;">
            <div class="row">
                <div class="md-form col-md-6">
                <input type="text" class="form-control" placeholder="Nip">  
                </div>
                <div class="md-form col-md-6">
                <input type="text" class="form-control" placeholder="Telpon">  
                </div>
                <div class="md-form col-md-6 pt-3">
                <input type="text" class="form-control" placeholder="Mapel">  
                </div>
                <div class="md-form col-md-6 pt-3">
                <input type="text" class="form-control" placeholder="Alamat">  
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- AKHIR Detail GURU -->

@stop