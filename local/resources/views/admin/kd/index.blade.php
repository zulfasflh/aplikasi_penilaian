@extends('layouts.master')
@section('content')

    <!-- JUMBOTRON -->
    <div class="container">
    <div class="jumbotron" style="background-color: transparent;margin-top:-40px;color:#797979">

        <!-- SESSION ALERT -->
        @if(session('sukses'))
          <div class="alert alert-success" role="alert">
            {{session('sukses')}}
          </div>
	      @endif
        <!-- END ALERT -->

        <h1 class="display-5">DATA KOMPETENSI DASAR</h1>
        <hr>
    </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- TABLE KOMPETENSI DASAR -->
    <div class="container" style="margin-top:-30px;">
    <form class="form-inline mr-auto" action="">
        <input type="text" class="form-control mr-sm-2" placeholder="Search" aria-label="Search">
        <i class="fas fa-search" aria-hidden="true"></i>
    </form>
    <button class="btn btn-outline-dark float-right"><i class="far fa-plus-square"></i> Tambah Data</button></h1>
            <table class="table shadow-sm">
                <thead class="thead-light" style="background-color:#797979;">
                <tr>
                <th scope="col">Nama</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            

                <tr>
                    <td>ff</td>
                    <td>ff</td>
                    <td>ff</td>
                    <td style="width:200px;">
                    <a href="#"><button class="btn btn-outline-dark" style="width:40px;"><i class="fas fa-search"></i></button></a> 
                    <a href="#"><button class="btn btn-outline-dark" style="width:40px;"><i class="fas fa-edit"></i></button></a>  
                    <a href="#" onclick="return confirm('Yakin ingin menghapus?')"><button class="btn btn-outline-dark" style="width:40px;"><i class="fas fa-trash-alt"></i></button></a>
                </td>
                </tr>
            </tbody>
            
        </table>

    </div>
    <!-- AKHIR TABLE KOMPETENSI DASAR -->

@stop