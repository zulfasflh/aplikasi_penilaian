<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Scores</title>

        <script defer src="{{ asset('/fontawesome/js/solid.js') }}"></script>
        <script defer src="{{ asset('/fontawesome/js/fontawesome.js') }}"></script>
        <script defer src="{{ asset('/mdb/js/mdb.min.js') }}"></script>
        <script defer src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script> 
        <link rel="stylesheet" type="text/css" href="{{ asset('/bootstrap/css/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/mdb/css/mdb.lite.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/mdb/css/mdb.min.css') }}">


        <!-- Styles -->
        <style>
        
            html, body {
                background-color: #f6f6f6;
                color: #636b6f;
                font-family: 'Scada';
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .btn{
                background-color: #506884;
                color: white;
                text-align: center;
            }

            .btn:hover{
                color:#f6f6f6;
            }

            h1{
               font-family: 'Iceberg';
               text-align: center;
               margin-bottom: 15%;
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }


            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .form-control{
                background-color: #f6f6f6;
            } 
            .m-b-md {
                margin-bottom: 30px;
            }.col-4{
                margin-top:5vh;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="container">
                <div class="row justify-content-right">
                    <div class="col-7">
                       <img class="img-fluid"src="{{ asset('img/rumah_new.png') }}" alt="">
                    </div>
                    <div class="col-4 rounded" style="box-shadow:29px;">
                    <h1>Masuk</h1>
                    <form>
                        <div class="md-form">
                            <i style="font-size:28px; margin-top:8px; margin-right:2px;" class="fas fa-users prefix"></i>
                            <input type="text" class="form-control" placeholder="Nama Pengguna">            
                        </div>
                        <div class="md-form">
                            <i style="font-size:28px; margin-top:8px;" class="fas fa-key prefix"></i>
                            <input type="password" class="form-control" placeholder="Kata Sandi">
                        </div>
                        <button type="submit" class="btn btn-sm btn-block" style="font-size:13px; border-radius:5px; margin-top:10%; box-shadow:none;">Masuk</button>
                    </form>
                    <footer style="font-size:15px; margin-top:25vh; margin-left:10px;">©copyright - 2020 | made with love by desyfazulfa</footer>
                    </div>
                </div>
                </div>
                
                </div>

                
            </div>
        </div>
    </body>
</html>
