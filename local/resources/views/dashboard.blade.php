@extends('layouts.master')
@section('content')
                
                <!-- START CONTAINER -->
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-sm-6">
                            <div class="card-box rounded" style="background-color:#506884;">
                                <div class="inner">
                                    <h3> 000 </h3>
                                </div>
                                <div class="icon">
                                    <!-- <i class="fa fa-graduation-cap"></i> -->
                                    <i class="fas fa-user-graduate" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="card-box-footer">Siswa</a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6">
                            <div class="card-box rounded" style="background-color:#506884;">
                                <div class="inner">
                                    <h3> 000 </h3>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-user-tie" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="card-box-footer">Guru</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="card-box rounded" style="background-color:#506884;">
                                <div class="inner">
                                    <h3> 000 </h3>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-book-reader" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="card-box-footer">Bidang Studi</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="card-box rounded" style="background-color:#506884;">
                                <div class="inner">
                                    <h3> 000 </h3>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-chalkboard-teacher"></i>
                                </div>
                                <a href="#" class="card-box-footer">Kompetensi Keahlian</a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END CONTAINER -->

                <br><br>

                <!-- START CHART -->
                    <div class="container">
                        <div id="chart">
                        
                        </div>
                    </div>
                <!-- END CHART -->
@stop

@section('footer')
<script src="{{asset('js/highcharts.js')}}"></script>
<script>
    Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Data'
        },
        xAxis: {
            categories: [
                'Siswa',
                'Guru',
                'Bidang Studi',
                'Kompetensi Keahlian', 
            ],
            crosshair: true
        },
        yAxis: {
            min:0,
            title: {
                text: 'Jumlah'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px;>{point.key}</span><table>',
            footerFormat: '</table>',
            shared: true,
            useHTML:true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Jumlah',
            data: [49,45,80,70]
        }]
    });
</script>
@stop