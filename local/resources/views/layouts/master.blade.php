<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Score's</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    
    <link rel="stylesheet" href="{{ asset('css/score.css') }}">

    <!-- Font Awesome JS -->
    <script defer src="{{asset('fontawesome/js/solid.js')}}"></script>
    <script defer src="{{asset('fontawesome/js/fontawesome.js')}}"></script>
</head>

<body>

        <!-- START WRAPPER -->
        <div class="wrapper">
            
            <!-- START Sidebar -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img class="logo" src="{{asset('img/score.png')}}">
                    <h4 class="title-side rounded" style="font-size:1.1em;"><center>username</center></h4>
                </div>

                <ul class="list-unstyled components">
                    
                    <li>
                        <a href="{{url('dashboard')}}"><i class="fas fa-home" style="color:#444444;"></i> Beranda</a>
                    </li>

                    <li>
                        <a href="{{url('siswa')}}"><i class="fas fa-user-graduate" style="color:#444444;"></i> Siswa</a>
                    </li>

                    <li>
                        <a href="{{url('guru')}}"><i class="fas fa-user-tie" style="color:#444444;"></i> Guru</a>
                    </li>

                    <li>
                        <a href="{{url('mapel')}}"><i class="fas fa-book-open" style="color:#444444;"></i> Mata Pelajaran</a>
                    </li>

                    <li>
                        <a href="{{url('bidang')}}"><i class="fas fa-book-reader" style="color:#444444;"></i> Bidang Studi</a>
                    </li>

                    <li>
                        <a href="{{url('kelas')}}"><i class="fas fa-users" style="color:#444444;"></i> Kelas</a>
                    </li>

                    <li>
                        <a href="{{url('kompetensi')}}"><i class="fas fa-chalkboard-teacher" style="color:#444444;"></i> Kompetensi Keahlian</a>
                    </li>

                    <li>
                        <a href="{{url('kd')}}"><i class="fas fa-chalkboard-teacher" style="color:#444444;"></i> Kompetensi Dasar</a>
                    </li>

                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-star" style="color:#444444;"></i> Nilai</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="{{url('kelolaNilai')}}">Kelola Nilai</a>
                            </li>
                            <li>
                                <a href="{{url('lihatNilai')}}">Lihat Nilai</a>
                            </li>
                            <li>
                                <a href="{{url('laporanNilai')}}">Laporan</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </nav>
            <!-- END SIDEBAR -->


            <!-- START CONTENT -->
            <div id="content">
                <!-- START NAVBAR -->
                <nav class="navbar navbar-expand-lg navbar-dark rounded" style="margin-top:-12px;height:55px;">

                    <button type="button" id="sidebarCollapse" style="background-color:#506884;" class="btn">
                        <i class="fas fa-align-justify" style="font-size:20px;color:#fff;"></i>
                    </button>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-align-justify" style="font-size:20px;color:#fff;"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto" style="margin-left:20px;">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('dashboard')}}">&nbsp;&nbsp;Keluar</a>
                            </li>
                            
                            </ul>
                        </div>
                </nav>  
                <!-- END NAVBAR -->
                
                @yield('content')

            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END WRAPPER -->

        <!-- FOOTER -->
        <div class="footer-copyright text-center py-3 pt-3" style="background-color:#f6f6f6;color:#b4b4b4;">
        Score's Copyright 2020 | made with <i class="fas fa-heart"></i> by desyfazulfa
        </div>
        <!-- END FOOTER -->


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="{{asset('js/jquery.slim.min.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Popper.JS -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- highcharts JS -->
    <script src="{{asset('js/highcharts.js')}}"></script>

    <script>
        $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            
        });

        });
    </script>

    @yield('footer')

</body>

</html>